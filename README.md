# Docker-ArchivesSpace-Database

Docker image containing a pre-loaded ArchivesSpace database, based on the
MySQL image.

The idea behind this Docker image is to decrease the startup time for an
ArchivesSpace Docker container, by loading the database from file. This allows
the ArchivesSpace Container to focus on starting ArchivesSpace, without being
required to run through all of the database initialization steps.


## Usage

This image inherits from the official `mysql:5.7` Docker image. The following
differences exist between this image and its parent image:

- The contents of the `/var/lib/mysql` directory were copied into the image
  when the image was created. The components of this directory will not be
  generated on startup. This included a blank instance of an ArchivesSpace
  database, version matched with the container's tag. The name of this
  database is `archivesspace`.
- The image no longer supports the "MYSQL_ALLOW_EMPTY_PASSWORD" environment
  variable. The mysql root user can be given no password by not specifying any
  of the other password variables.
- The image does not require any environment variables to be set.
- Specifying the `MYSQL_DATABASE` environment variable will not re-create the
  database. If you want to use the `MYSQL_USER` and `MYSQL_PASSWORD`
  variables to create an ArchivesSpace MySQL user, you will need to specify
  the `MYSQL_DATABASE` variable, with the value `archivesspace`.

Other than these differences, the container should behave exactly the same
as the `mysql:5.7` image, except with pre-loaded data.

```bash
docker run -d -p 3306:3306 austintschaffer/aspace-db:2.3.2

# It takes a few seconds to spin up.

echo 'select * from archivesspace.schema_info;' | mysql -h127.0.0.1 -uroot
# version
# 97
```

Example for using docker-compose:

```yml
version: '2'
services:
  db:
    image: austintschaffer/aspace-db:2.4.1
    command: --character-set-server=utf8 --collation-server=utf8_unicode_ci --innodb_buffer_pool_size=2G --innodb_buffer_pool_instances=2 --lower_case_table_names=1
    ports:
      - "3306:3306"
    environment:
      MYSQL_ROOT_PASSWORD: 123456
      MYSQL_DATABASE: archivesspace
      MYSQL_USER: as
      MYSQL_PASSWORD: as123
  aspace:
    image: archivesspace/archivesspace:2.4.1
    ports:
      - "8080:8080"
      - "8081:8081"
      - "8082:8082"
      - "8089:8089"
      - "8090:8090"
    environment:
      APPCONFIG_DB_URL: 'jdbc:mysql://db:3306/archivesspace?useUnicode=true&characterEncoding=UTF-8&user=as&password=as123'
    depends_on:
      - db
```


## Updating

To update, run the `docker-compose up` from within the `build_aspace_db`
directory. This will start a mysql instance, along with a container that runs
through the relevant ArchivesSpace db migrate scripts. Once the ArchivesSpace
container runs to completion, you can use `sudo docker cp` to pull the
`/var/lib/mysql` directory from the container that houses the MySQL instance.
Use that to create a new v2.X.X directory for that version of ArchivesSpace.

Don't forget to push them to the Docker hub.
