#!/bin/bash

#
# Wait for MySQL server to become responsive
#

if [[ -z $MYSQL_DELAY ]]; then
  MYSQL_DELAY=60
fi

if [[ -z $MYSQL_CHECK_INTERVAL ]]; then
  MYSQL_CHECK_INTERVAL=5
fi

counter=0

echo "Waiting up to $MYSQL_DELAY seconds for MySQL. Checking every $MYSQL_CHECK_INTERVAL seconds."

while ! mysql -h"$DB_ADDR" --port=3306 --user=root --password="$MYSQL_ROOT_PASSWORD" -e "show databases;" > /dev/null 2>&1; do
    if [ $counter -gt $MYSQL_DELAY ]; then
        >&2 echo "We have been waiting for MySQL too long already; failing."
        exit 1
    fi;

    >&1 echo "Connection failed, retrying in $MYSQL_CHECK_INTERVAL seconds."
    counter=`expr $counter + $MYSQL_CHECK_INTERVAL`
    sleep $MYSQL_CHECK_INTERVAL
done

#
# Init Database and Start ArchivesSpace
#

/archivesspace/scripts/setup-database.sh

