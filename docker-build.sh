#!/bin/bash

cp ./common/* v232/
sudo docker build -t austintschaffer/aspace-db:2.3.2 ./v232

cp ./common/* v240/
sudo docker build -t austintschaffer/aspace-db:2.4.0 ./v240

cp ./common/* v241/
sudo docker build -t austintschaffer/aspace-db:2.4.1 ./v241

cp ./common/* v250/
sudo docker build -t austintschaffer/aspace-db:2.5.0 ./v250

sudo docker tag austintschaffer/aspace-db:2.5.0 austintschaffer/aspace-db:latest

